# DRAFT FOR PROJECT TIC TAC TOE, Human vs Human
my_board = [0, 1, 2, 3, 4, 5, 6, 7, 8]

def print_board(my_board):
    print(my_board[0:3])
    print(my_board[3:6])
    print(my_board[6:])

# slot = value selected by the user
# value = either "X" or "O". "X" starts by default.
# my_board = list of board

print_board(my_board)

def play(slot, value, my_board):
    while True:
        try:
            my_board.remove(slot)  # remove the slot that the user selected
            my_board.insert(slot, value)  # the playing symbol will be inserted in the previously removed slot
            break  # Exit the loop if the code in the try block is successful
        except ValueError:
            print("Please select a valid position")

def get_valid_position(prompt_slot):
    while True:
        try:
            position = int(input(prompt_slot))
            if 0 <= position <= 8:
                return position
            else:
                print("Please select a valid position (0 to 8)")
        except ValueError:
            print("Please enter a valid integer")

def free_position(position): # checks if the chosen position was already chosen before
    while my_board[position] == "X" or my_board[position] == "O":
        print("You can't choose this position, please choose another one")
        position = get_valid_position("Enter new position: ")
    return position

# def victory_conditions(): # checks if victory conditions are met
#     # Checking if 'X' won
#     for i in range(0, 9, 3): # check vertical alignments
#         if my_board[i] == my_board[i + 1] == my_board[i + 2] == "X":
#             print("'X' won the match, congrats!")
#             return True
#
#     for i in range(3): # check vertical alignments
#         if my_board[i] == my_board[i + 3] == my_board[i + 6] == "X":
#             print("'X' won the match, congrats!")
#             return True
#
#     if my_board[0] == my_board[4] == my_board[8] == "X" or my_board[2] == my_board[4] == my_board[6] == "X":
#         print("'X' won the match, congrats!") # check diagonal alignments
#         return True
#
#     # Checking if 'O' won
#     for i in range(0, 9, 3):  # check vertical alignments
#          if my_board[i] == my_board[i + 1] == my_board[i + 2] == "O":
#             print("'O' won the match, congrats!")
#             return True
#
#     for i in range(3):  # check vertical alignments
#         if my_board[i] == my_board[i + 3] == my_board[i + 6] == "O":
#             print("'O' won the match, congrats!")
#             return True
#
#     if my_board[0] == my_board[4] == my_board[8] == "O" or my_board[2] == my_board[4] == my_board[6] == "O":
#         print("'O' won the match, congrats!")  # check diagonal alignments
#         return True
#
#     # Check for a draw
#     for i in range(9):
#         if my_board[i] == "X" or my_board[i] == "O":
#             print("The match ended in a draw, well played by both!")
#             return True

position_1 = get_valid_position("Enter position for 'X': ")

play(position_1, "X", my_board)
print_board(my_board)

position_2 = get_valid_position("Enter position for 'O': ")
position_2 = free_position(position_2)
play(position_2, "O", my_board)
print_board(my_board)

position_3 = get_valid_position("Enter position for 'X': ")
position_3 = free_position(position_3)
play(position_3, "X", my_board)
print_board(my_board)

position_4 = get_valid_position("Enter position for 'O': ")
position_4 = free_position(position_4)
play(position_4, "O", my_board)
print_board(my_board)

position_5 = get_valid_position("Enter position for 'X': ")
position_5 = free_position(position_5)
play(position_5, "X", my_board)
print_board(my_board)
# victory_conditions()
if my_board[0] == my_board[1] and my_board[1] == my_board[2] and my_board[2] == "X":
    print("'X' won the match, congrats!")
elif my_board[3] == my_board[4] and my_board[4] == my_board[5] and my_board[5] == "X":
    print("'X' won the match, congrats!")
elif my_board[6] == my_board[7] and my_board[7] == my_board[8] and my_board[8] == "X":
    print("'X' won the match, congrats!")
elif my_board[0] == my_board[3] and my_board[3] == my_board[6] and my_board[6] == "X":
    print("'X' won the match, congrats!")
elif my_board[1] == my_board[4] and my_board[4] == my_board[7] and my_board[7] == "X":
    print("'X' won the match, congrats!")
elif my_board[2] == my_board[5] and my_board[5] == my_board[8] and my_board[8] == "X":
    print("'X' won the match, congrats!")
elif my_board[0] == my_board[4] and my_board[4] == my_board[8] and my_board[8] == "X":
    print("'X' won the match, congrats!")
elif my_board[2] == my_board[4] and my_board[4] == my_board[6] and my_board[6] == "X":
    print("'X' won the match, congrats!")
else:
    position_6 = get_valid_position("Enter position for 'O': ")
    position_6 = free_position(position_6)
    play(position_6, "O", my_board)
    print_board(my_board)
    # victory_conditions()
    if my_board[0] == my_board[1] and my_board[1] == my_board[2] and my_board[2] == "X":
        print("'X' won the match, congrats!")
    elif my_board[3] == my_board[4] and my_board[4] == my_board[5] and my_board[5] == "X":
        print("'X' won the match, congrats!")
    elif my_board[6] == my_board[7] and my_board[7] == my_board[8] and my_board[8] == "X":
        print("'X' won the match, congrats!")
    elif my_board[0] == my_board[3] and my_board[3] == my_board[6] and my_board[6] == "X":
        print("'X' won the match, congrats!")
    elif my_board[1] == my_board[4] and my_board[4] == my_board[7] and my_board[7] == "X":
        print("'X' won the match, congrats!")
    elif my_board[2] == my_board[5] and my_board[5] == my_board[8] and my_board[8] == "X":
        print("'X' won the match, congrats!")
    elif my_board[0] == my_board[4] and my_board[4] == my_board[8] and my_board[8] == "X":
        print("'X' won the match, congrats!")
    elif my_board[2] == my_board[4] and my_board[4] == my_board[6] and my_board[6] == "X":
        print("'X' won the match, congrats!")
    else:
        position_7 = get_valid_position("Enter position for 'X': ")
        position_7 = free_position(position_7)
        play(position_7, "X", my_board)
        print_board(my_board)
        # victory_conditions()
        if my_board[0] == my_board[1] and my_board[1] == my_board[2] and my_board[2] == "X":
            print("'X' won the match, congrats!")
        elif my_board[3] == my_board[4] and my_board[4] == my_board[5] and my_board[5] == "X":
            print("'X' won the match, congrats!")
        elif my_board[6] == my_board[7] and my_board[7] == my_board[8] and my_board[8] == "X":
            print("'X' won the match, congrats!")
        elif my_board[0] == my_board[3] and my_board[3] == my_board[6] and my_board[6] == "X":
            print("'X' won the match, congrats!")
        elif my_board[1] == my_board[4] and my_board[4] == my_board[7] and my_board[7] == "X":
            print("'X' won the match, congrats!")
        elif my_board[2] == my_board[5] and my_board[5] == my_board[8] and my_board[8] == "X":
            print("'X' won the match, congrats!")
        elif my_board[0] == my_board[4] and my_board[4] == my_board[8] and my_board[8] == "X":
            print("'X' won the match, congrats!")
        elif my_board[2] == my_board[4] and my_board[4] == my_board[6] and my_board[6] == "X":
            print("'X' won the match, congrats!")
        else:
            position_8 = get_valid_position("Enter position for 'O': ")
            position_8 = free_position(position_8)
            play(position_8, "O", my_board)
            print_board(my_board)
            # victory_conditions()
            if my_board[0] == my_board[1] and my_board[1] == my_board[2] and my_board[2] == "X":
                print("'X' won the match, congrats!")
            elif my_board[3] == my_board[4] and my_board[4] == my_board[5] and my_board[5] == "X":
                print("'X' won the match, congrats!")
            elif my_board[6] == my_board[7] and my_board[7] == my_board[8] and my_board[8] == "X":
                print("'X' won the match, congrats!")
            elif my_board[0] == my_board[3] and my_board[3] == my_board[6] and my_board[6] == "X":
                print("'X' won the match, congrats!")
            elif my_board[1] == my_board[4] and my_board[4] == my_board[7] and my_board[7] == "X":
                print("'X' won the match, congrats!")
            elif my_board[2] == my_board[5] and my_board[5] == my_board[8] and my_board[8] == "X":
                print("'X' won the match, congrats!")
            elif my_board[0] == my_board[4] and my_board[4] == my_board[8] and my_board[8] == "X":
                print("'X' won the match, congrats!")
            elif my_board[2] == my_board[4] and my_board[4] == my_board[6] and my_board[6] == "X":
                print("'X' won the match, congrats!")
            else:
                position_9 = get_valid_position("Enter position for 'X': ")
                position_9 = free_position(position_9)
                play(position_9, "X", my_board)
                print_board(my_board)
                # victory_conditions()
                if my_board[0] == my_board[1] and my_board[1] == my_board[2] and my_board[2] == "X":
                    print("'X' won the match, congrats!")
                elif my_board[3] == my_board[4] and my_board[4] == my_board[5] and my_board[5] == "X":
                    print("'X' won the match, congrats!")
                elif my_board[6] == my_board[7] and my_board[7] == my_board[8] and my_board[8] == "X":
                    print("'X' won the match, congrats!")
                elif my_board[0] == my_board[3] and my_board[3] == my_board[6] and my_board[6] == "X":
                    print("'X' won the match, congrats!")
                elif my_board[1] == my_board[4] and my_board[4] == my_board[7] and my_board[7] == "X":
                    print("'X' won the match, congrats!")
                elif my_board[2] == my_board[5] and my_board[5] == my_board[8] and my_board[8] == "X":
                    print("'X' won the match, congrats!")
                elif my_board[0] == my_board[4] and my_board[4] == my_board[8] and my_board[8] == "X":
                    print("'X' won the match, congrats!")
                elif my_board[2] == my_board[4] and my_board[4] == my_board[6] and my_board[6] == "X":
                    print("'X' won the match, congrats!")
                else:
                    print("The match ended in a draw, well played by both!")